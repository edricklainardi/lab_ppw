from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('bio/', views.bio, name='bio'),
    path('achievements/', views.achievements, name='achievements'),
    path('contact/', views.contact, name='contact'),
    path('gallery/', views.gallery, name='gallery'),
    path('guestbook/', views.bukutamu, name='bukutamu'),
    path('schedule/', views.schedule, name='schedule'),
    path('scheduletest/', views.scheduletest, name='scheduletest'),
    path('delete/', views.delete, name='delete')
]

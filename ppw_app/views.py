from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from ppw_app.forms import HomeForm
from ppw_app.models import Schedule

def home(request):
    return render(request, 'ppw_app/home.html', {})

def bio(request):
    return render(request, 'ppw_app/bio.html', {})

def achievements(request):
    return render(request, 'ppw_app/achievements.html', {})

def contact(request):
    return render(request, 'ppw_app/contact.html', {})

def gallery(request):
    return render(request, 'ppw_app/gallery.html', {})

def bukutamu(request):
    return render(request, 'ppw_app/bukutamu.html', {})

def schedule(request):
    form = HomeForm(request.POST)
    if form.is_valid():
        schedule = form.save(commit=False)
        schedule.save()
        form = HomeForm()
        return render(request, 'ppw_app/schedule.html', {'form': form})
    args = {'form': form}
    return render(request, 'ppw_app/schedule.html', args)

def scheduletest(request):
    schedules = Schedule.objects.all()
    args = {'schedules': schedules}
    return render(request, 'ppw_app/scheduletest.html', args)

def delete(request):
    objects = Schedule.objects.all()
    objects.delete()
    return render(request, 'ppw_app/scheduletest.html', {})

# Create your views here.

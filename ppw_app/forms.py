from django import forms
from ppw_app.models import Schedule

class HomeForm(forms.ModelForm):
    DAYS = (
        ('Mon', 'Monday'),
        ('Tue', 'Tuesday'),
        ('Wed', 'Wednesday'),
        ('Thur', 'Thursday'),
        ('Fri', 'Friday'),
        ('Sat', 'Saturday'),
        ('Sun', 'Sunday'),
    )
    name = forms.CharField(max_length=50)
    days = forms.ChoiceField(choices=DAYS)
    time = forms.TimeField(widget = forms.TimeInput(attrs = {'type' : 'time'}))
    date = forms.DateField(widget = forms.DateInput(attrs = {'type' : 'date'}))
    place= forms.CharField(max_length=50)
    category= forms.CharField(max_length=50)

    class Meta:
        model = Schedule
        fields = ('name','days','time','date','place','category')

from django.db import models

class Schedule(models.Model):
    name = models.CharField(max_length=500)
    days = models.CharField(max_length=500)
    time = models.CharField(max_length=500)
    date = models.CharField(max_length=500)
    place= models.CharField(max_length=500)
    category= models.CharField(max_length=500)
